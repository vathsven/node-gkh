'use strict';

const fs = require('fs');
const mysql = require('mysql2/promise');
const colors = require('colors');
const _ = require('lodash');
const utils = require('./lib/utils');
const house = require('./lib/libhouse');
const org = require('./lib/liborg');
const mongo = require('./lib/libmongo');
const log4js = require('log4js');
const addr = require('./lib/libaddr');
const term = require('terminal-kit').terminal;

const {chain}  = require('stream-chain');
const {parser} = require('stream-json');
const {pick}   = require('stream-json/filters/Pick');
const {streamArray} = require('stream-json/streamers/StreamArray');
const AlignStream = require('utf8-align-stream');


// CLI params ?
var ForceRegionParse = null;
if (process.argv[2] != 'undefined') {
  ForceRegionParse = process.argv[2];
}


/**
  Insert settlement, street, house, organization, employee
  @param orgObj {object} Organization object params
  @param conn {object} mysql connection
  @param logger {object} Logger object
*/
const CheckOrgAwait = async (orgObj, conn, logger) => {

  var parsed, houseNumber;
  var streetNotFound = {};
  var connection = conn;

  var search = {};
  //search = utils.FormatOrgSearch(orgObj, logger);
  search = addr.FormatSearch(orgObj);

  if (!search) {
    return;
  }

  term.saveCursor();
  term.eraseLine();
  term(orgObj['guid']);
  term.restoreCursor();

  // Get or insert new `t4qz8_settlement.sub_region_id`
  if (search.area !== undefined) {
    search.sub_region_id = await addr.GetSubRegion(search, connection, logger);
  } else {
    search.sub_region_id = undefined;
  }

  // Get or insert `t4qz8_settlement`
  search.settlement_id = await addr.GetSettlement(search, connection, logger);
  if (search.settlement_id === undefined) return;

  // Get or insert `t4qz8_street`
  search.street_id = await addr.GetStreet(search, connection, logger);
  if (search.street_id === undefined) return;

  // House insert or update
  search.house_id = await house.Insert(search, connection, logger);
  if (search.house_id === undefined) return;

  // Organization insert or update
  search.org_id = await org.Insert(search, connection, logger, orgObj);
  if (search.org_id === undefined) return;

  // Employee
  search.organization_employee_id = await org.InsertEmployee(
      search, connection, logger, orgObj
    );

}


// Init
async function main() {

  if (ForceRegionParse) {

    console.log("\nPARSING ORG".green, ForceRegionParse);

    // Mongo installed ?
    // Ready to insert
    var MONGODB_ACTIVE = true;

    try {
      var collection = await mongo.Connect('gkh','organizations');
    } catch(e) {
      MONGODB_ACTIVE = false;
    }

    // Mysql
    const connection = await mysql.createConnection({
      host     : process.env.GKH_MYSQL_HOST,
      user     : process.env.GKH_MYSQL_USER,
      password : process.env.GKH_MYSQL_PASSWORD,
      database : process.env.GKH_MYSQL_DATABASE
    });

    log4js.configure({
      appenders: { org: { type: 'file', filename: 'org_'+ForceRegionParse+'.log' } },
      categories: { default: { appenders: ['org'], level: 'debug' } }
    });

    var logger = log4js.getLogger('org');

    var pipeline = chain([
      fs.createReadStream('./json/extracted/'+ForceRegionParse+'/organizations.json'),
      new AlignStream(),
      parser(),
      pick({filter: 'organizations'}),
      streamArray(),
      async data => {
        if (MONGODB_ACTIVE) {
          await mongo.DeleteThenInsertObj(data.value);
        }
        await CheckOrgAwait(data.value, connection, logger);
      }
    ]);

    let counter = 0;
    pipeline.on('data', () => ++counter);
    pipeline.on('end', async () => {
      if (MONGODB_ACTIVE) await mongo.Close();
      await connection.end();
      term.saveCursor();
      term.eraseLine();
      term("FINISHED ORG ".magenta, ForceRegionParse);
      term.restoreCursor();
    });

  } else {
    console.log("ERR: NO REGION PROVIDED".red);
  }
  return;
}

main();