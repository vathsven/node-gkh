'use strict';

const utils = require('./utils');

const self = this;


exports.FormatSearch = (orgObj) => {

  let objKey, search = {};

  // Organization ??
  if (orgObj.hasOwnProperty('address')) {
    objKey = 'address';
  }

  // House ??
  if (orgObj.hasOwnProperty('factualAddress')) {
    objKey = 'factualAddress';
  }
  if (orgObj.hasOwnProperty('authorityFactualAddress')) {
    objKey = 'authorityFactualAddress';
  }

  search = self.BuildSearchObj(orgObj[objKey]);
  if (search !== undefined) 
    search.guid = orgObj['guid'];
  return search;
}


exports.BuildSearchObj = (addrObj) => {

  let search = {};

  [search.houseNumber, search.buildingNumber, search.structNumber, search.houseGuid] = 
    utils.GetHouseNumber(addrObj);
  if (search.houseNumber === undefined && search.buildingNumber === undefined 
    && search.structNumber === undefined) return undefined;

  if (addrObj.hasOwnProperty('city')) {
    [search.city, search.cityOff] = utils.NameFlipDamaged(
        addrObj['city']['formalName'],
        addrObj['city']['offName']
      );
    search.cityGuid = addrObj['city']['guid'];
    search.cityAddr = addrObj['formattedAddress'];
    search.cityType = addrObj['city']['shortName'] + '.';
  }

  if (addrObj.hasOwnProperty('settlement')) {
    [search.city, search.cityOff] = utils.NameFlipDamaged(
        addrObj['settlement']['formalName'],
        addrObj['settlement']['offName']
      );
    search.cityGuid = addrObj['settlement']['guid'];
    search.cityAddr = addrObj['formattedAddress'];
    search.cityType = addrObj['settlement']['shortName'] + '.';
  }

  // case: settlement && city -> `parent_settlement_id`
  if (addrObj.hasOwnProperty('settlement') && addrObj.hasOwnProperty('city')) {
    [search.city, search.cityOff] = utils.NameFlipDamaged(
        addrObj['settlement']['formalName'],
        addrObj['settlement']['offName']
      );
    search.cityGuid = addrObj['settlement']['guid'];
    search.cityAddr = addrObj['formattedAddress'];
    search.cityType = addrObj['settlement']['shortName'] + '.';

    [search.parentCity, search.parentCityOff] = utils.NameFlipDamaged(
        addrObj['city']['formalName'],
        addrObj['city']['offName']
      );
    search.parentCityGuid = addrObj['city']['guid'];
    search.parentCityType = addrObj['city']['shortName'] + '.';
  }

  // street
  if (addrObj.hasOwnProperty('street')) {
    [search.street, search.streetOff] = utils.NameFlipDamaged(
        addrObj['street']['formalName'],
        addrObj['street']['offName']
      );
    search.streetGuid = addrObj['street']['guid'];
    search.streetType = addrObj['street']['shortName'] + '.';
  } else {
    search.street = 'NULL';
  }

  // area -> `sub_region_id`
  if (addrObj.hasOwnProperty('area')) {
    [search.area, search.areaOff] = utils.NameFlipDamaged(
        addrObj['area']['formalName'],
        addrObj['area']['offName']
      );
    //search.area = orgObj['authorityFactualAddress']['region']['formalName'];
    search.areaType = addrObj['area']['shortName'] + '.';
  }

  if (addrObj.hasOwnProperty('region')) {
    search.regionNumber = addrObj['region']['regionCode'];
    if (search.regionNumber == 50) {
      search.regionNumber = 77;
      search.real_region = 50;
    } else {
      search.real_region = 0;
    }
  }

  // mytischi hardfix
  if (search.city == 'Мытищи' && (search.cityType == 'г.о.' || search.cityType == 'г.')) {
    search.cityType = 'г.';
  }
  if (search.parentCity == 'Мытищи' 
    && (search.parentCityType == 'г.о.' || search.parentCityType == 'г.')) {
    search.parentCityType = 'г.';
  }

  // replace double dot
  var re = /\.\./g;
  if (search.areaType !== undefined)
    search.areaType = search.areaType.replace(re, '.');
  if (search.cityType !== undefined)
    search.cityType = search.cityType.replace(re, '.');
  if (search.parentCityType !== undefined)
    search.parentCityType = search.parentCityType.replace(re, '.');
  if (search.streetType !== undefined)
    search.streetType = search.streetType.replace(re, '.');

  // areaType hard replace
  if (search.areaType == 'р-н.') {
    search.areaType = 'район';
  }
  if (search.areaType == 'п.') {
    search.areaType = 'п';
  }

  // streetType hard replace
  if (search.streetType == 'ал.') {
    search.streetType = 'аллея.';
  }
  if (search.streetType == 'пер..') {
    search.streetType = 'пер.';
  }
  if (search.streetType == 'пр-кт.') {
    search.streetType = 'просп.';
  }
  if (search.streetType == 'тракт.') {
    search.streetType = 'тр.';
  }
  if (search.streetType == 'проезд.') {
    search.streetType = 'пр.';
  }
  if (search.streetType == 'п.') {
    search.streetType = 'пос.';
  }

  return search;
}

/**
  Get or insert `t4qz8_sub_region`
  @param search {object} Search params
  @param conn {object} mysql connection
  @param logger {object} Logger object
*/
exports.GetSubRegion = async (search, connection, logger) => {

  var [rows, fields] = await connection.query(
    `SELECT id FROM t4qz8_sub_region WHERE name='${search.area}' 
    AND type_id=(SELECT id FROM t4qz8_sub_region_type 
    WHERE name='${search.areaType}' AND region_id='${search.regionNumber}');`
    );

  if (rows.length < 1) {
    try {
      var [rows1, fields1] = await connection.query(
        `INSERT INTO t4qz8_sub_region (region_id, type_id, name) 
        VALUES ('${search.regionNumber}', (SELECT id FROM t4qz8_sub_region_type 
        WHERE name='${search.areaType}'), '${search.area}');`
        );

      return rows1.insertId;

    } catch(e) {
      logger.debug("SUB REGION", search.areaType, e);
      return undefined;

    }
  } else {

    return rows[0].id;

  }
}


/**
  @param search {object} Search params
  @param conn {object} mysql connection
  @param logger {object} Logger object
*/
exports.GetStreet = async (search, connection, logger) => {

  if (search.street != 'NULL') {
    var [rows4, fields4] = await connection.query(
      `SELECT id FROM t4qz8_street WHERE settlement_id='${search.settlement_id}' 
      AND name='${search.street}' AND type_id=(SELECT id FROM t4qz8_street_type 
      WHERE short_name='${search.streetType}');`);
    if (rows4.length < 1) {
      try {
        var [rows5, fields5] = await connection.query(
          `INSERT INTO t4qz8_street (settlement_id, type_id, name, published, guid) 
          VALUES ('${search.settlement_id}', (SELECT id FROM t4qz8_street_type 
          WHERE short_name='${search.streetType}'), '${search.street}', '1', 
          '${search.streetGuid}') ON DUPLICATE KEY UPDATE 
          settlement_id='${search.settlement_id}', guid='${search.streetGuid}';`);
        search.street_id = rows5.insertId;
      } catch(e) {
        logger.debug("STREET", e);
        search.street_id = 'NULL';
      }
    } else {
      search.street_id = rows4[0].id;
      try {
        await connection.query(
          `UPDATE t4qz8_street SET guid='${search.streetGuid}' 
          WHERE id=${search.street_id}`
          );
      } catch(e) {
        logger.debug("STREET", e);
      }
    }
  } else {
    search.street_id = 'NULL';
  }

  return search.street_id;
}


exports.GetSettlement = async (search, connection, logger) => {

  if (search.city === undefined && search.cityGuid === undefined) {
    logger.debug("HOUSE UNDEFINED", search);
    return undefined;
  }

  // guid ?
  let q = `SELECT id FROM t4qz8_settlement WHERE guid='${search.cityGuid}'`;
  var [rows01, fields01] = await connection.query(q);
  if (rows01.length > 0) {
    return rows01[0].id;     // success, found by guid
  }

  // Parent settlement
  if (search.parentCity !== undefined) {
    q = `SELECT id FROM t4qz8_settlement WHERE guid='${search.parentCityGuid}'`;
    [rows01, fields01] = await connection.query(q);
    if (rows01.length > 0) {
      search.parent_settlement_id = rows01[0].id;
    } else {

      // TODO: sub_region_id add ?
      q = `SELECT id FROM t4qz8_settlement WHERE name='${search.parentCity}' AND 
        region_id='${search.regionNumber}' AND available=1 
        AND (parent_settlement_id=0 OR parent_settlement_id IS NULL) 
        AND type_id=(SELECT id FROM t4qz8_settlement_type 
        WHERE short_name='${search.parentCityType}');`;

        var [rows02, fields02] = await connection.query(q);
        if (rows02.length > 0) {
          search.parent_settlement_id = rows02[0].id;
        } else {
          // Insert settlement
          try {
            var [rows3, fields3] = await connection.query(
              `INSERT INTO t4qz8_settlement (region_id, type_id, name, 
              available, sort, real_region, guid) VALUES 
              ('${search.regionNumber}', (SELECT id FROM t4qz8_settlement_type 
              WHERE short_name='${search.parentCityType}'), '${search.parentCity}', 
              1, '999', '${search.real_region}', '${search.parentCityGuid}');`);
            search.parent_settlement_id = rows3.insertId;
            logger.debug("PARENT SETTLEMENT INSERT", search.rows3);
          } catch(e) {
            logger.debug("PARENT SETTLEMENT ERROR", e);
          }
        }
    }
  }

  // Catch record without sub_region_id and parent_settlement_id
  q = `SELECT id FROM t4qz8_settlement WHERE name='${search.city}' AND 
  region_id='${search.regionNumber}' AND sub_region_id IS NULL 
  AND (parent_settlement_id=0 OR parent_settlement_id IS NULL) 
  AND guid IS NULL AND type_id=(SELECT id FROM t4qz8_settlement_type 
  WHERE short_name='${search.cityType}');`;

  var [rows4, fields4] = await connection.query(q);

  if (rows4.length > 0) {

    let sub_id = rows4[0].id;

    // Update guid, sub_region_id, parent_settlement_id if exists
    q = `UPDATE t4qz8_settlement SET guid='${search.cityGuid}' WHERE id=${sub_id}`;

    if (search.sub_region_id !== undefined) {
      q = `UPDATE t4qz8_settlement SET sub_region_id=${search.sub_region_id}, 
      guid='${search.cityGuid}' WHERE id=${sub_id}`;
    }
    if (search.sub_region_id !== undefined && search.parent_settlement_id !== undefined) {
      q = `UPDATE t4qz8_settlement SET sub_region_id=${search.sub_region_id}, 
      guid='${search.cityGuid}', parent_settlement_id=${search.parent_settlement_id} 
      WHERE id=${sub_id}`;
    }

    logger.debug("DOUBLES", q);

    try {
      await connection.query(q);
    } catch(e) {
      logger.debug(e);
    }

    return sub_id;
  }

  // Search by params
  let query_sub_region_id = search.sub_region_id === undefined 
    ? 'AND sub_region_id IS NULL' : `AND sub_region_id=${search.sub_region_id}`;

  let query_parent = search.parent_settlement_id === undefined 
    ? 'AND (parent_settlement_id=0 OR parent_settlement_id IS NULL)' 
    : `AND parent_settlement_id=${search.parent_settlement_id}`;

  q = `SELECT id FROM t4qz8_settlement WHERE name='${search.city}' AND 
  region_id='${search.regionNumber}' ${query_sub_region_id} ${query_parent} AND 
  guid IS NULL AND type_id=(SELECT id FROM t4qz8_settlement_type WHERE 
  short_name='${search.cityType}');`;

  var [rows4, fields4] = await connection.query(q);

  if (rows4.length > 0) {
    search.settlement_id = rows4[0].id;
    try {
      await connection.query(
        `UPDATE t4qz8_settlement SET guid='${search.cityGuid}' 
        WHERE id=${search.settlement_id}`
        );
    } catch(e) {
      //pass
    }
    return search.settlement_id;
  } else {
    // Insert new settlement
    try {
      if (search.sub_region_id === undefined)
        search.sub_region_id = 'NULL';
      if (search.parent_settlement_id === undefined)
        search.parent_settlement_id = 'NULL';
      var [rows4, fields4] = await connection.query(
          `INSERT INTO t4qz8_settlement (region_id, type_id, name, 
          sub_region_id, available, sort, real_region, guid, parent_settlement_id) 
          VALUES ('${search.regionNumber}', (SELECT id FROM t4qz8_settlement_type 
          WHERE short_name='${search.cityType}'), '${search.city}', 
          ${search.sub_region_id}, 1, '999', '${search.real_region}', 
          '${search.cityGuid}', ${search.parent_settlement_id});`);

      return rows4.insertId;

    } catch(e) {
      logger.debug("SETTLEMENT INSERT ERROR", search, e);
    }
  }

  return undefined;
}

/**
  Get or insert `t4qz8_settlement`
  @param search {object} Search params
  @param conn {object} mysql connection
  @param logger {object} Logger object
*/
exports.___GetSettlement = async (search, connection, logger) => {

  // Check settlement without sub_region && without guid
  // Set guid field or set guid and sub_region_id if exist
  let q = `SELECT id FROM t4qz8_settlement WHERE name='${search.city}' AND 
  region_id='${search.regionNumber}' AND sub_region_id IS NULL AND 
  guid IS NULL AND type_id=(SELECT id FROM t4qz8_settlement_type WHERE 
  short_name='${search.cityType}');`;

  var [rows01, fields01] = await connection.query(q);

  // Found: no `guid`, no `sub_region_id`
  if (rows01.length > 0) {
    if (search.sub_region_id === undefined) {
      try {
        var [rows010, fields010] = await connection.query(`
          UPDATE t4qz8_settlement SET guid='${search.cityGuid}' 
          WHERE name='${search.city}' AND region_id='${search.regionNumber}' 
          AND sub_region_id IS NULL AND guid IS NULL 
          AND type_id=(SELECT id FROM t4qz8_settlement_type 
          WHERE short_name='${search.cityType}');`);
      } catch(e) {
        logger.debug("SETTLEMENT", e);
      }
    } else {
      try {
        var [rows010, fields010] = await connection.query(`
          UPDATE t4qz8_settlement SET guid='${search.cityGuid}', 
          sub_region_id='${search.sub_region_id}' WHERE name='${search.city}' 
          AND region_id='${search.regionNumber}' AND sub_region_id IS NULL 
          AND guid IS NULL AND type_id=(SELECT id FROM t4qz8_settlement_type 
          WHERE short_name='${search.cityType}');`);
      } catch(e) {
        logger.debug("SETTLEMENT", e);
      }
    }
  }

  if (search.sub_region_id !== undefined) {
    var [rows2, fields2] = await connection.query(
      `SELECT id FROM t4qz8_settlement WHERE (name='${search.city}' 
      AND region_id='${search.regionNumber}' 
      AND sub_region_id='${search.sub_region_id}' 
      AND type_id=(SELECT id FROM t4qz8_settlement_type 
      WHERE short_name='${search.cityType}')) or guid='${search.cityGuid}';`
      );
    if (rows2.length < 1) {
      try {
        var [rows3, fields3] = await connection.query(
          `INSERT INTO t4qz8_settlement (region_id, type_id, name, 
          sub_region_id, available, sort, real_region, guid) VALUES 
          ('${search.regionNumber}', (SELECT id FROM t4qz8_settlement_type 
          WHERE short_name='${search.cityType}'), '${search.city}', 
          '${search.sub_region_id}', 1, '999', '${search.real_region}', 
          '${search.cityGuid}') ON DUPLICATE KEY UPDATE guid='${search.cityGuid}', 
          name='${search.city}';`);
        search.settlement_id = rows3.insertId;
      } catch(e) {
        logger.debug("SETTLEMENT", e);
        // will be no settlement_id
        // process next org
        return undefined;
      }
    } else {
      search.settlement_id = rows2[0].id;
    }
  } else {
    var [rows2, fields2] = await connection.query(
      `SELECT id FROM t4qz8_settlement WHERE (name='${search.city}' 
      AND region_id='${search.regionNumber}' AND sub_region_id IS NULL 
      AND type_id=(SELECT id FROM t4qz8_settlement_type 
      WHERE short_name='${search.cityType}')) or guid='${search.cityGuid}';`
      );
    if (rows2.length < 1) {
      try {
        var [rows3, fields3] = await connection.query(`
          INSERT INTO t4qz8_settlement (region_id, type_id, name, sub_region_id, 
          available, sort, real_region, guid) VALUES ('${search.regionNumber}', 
          (SELECT id FROM t4qz8_settlement_type 
          WHERE short_name='${search.cityType}'), '${search.city}', NULL, 1, 
          '999', '${search.real_region}', '${search.cityGuid}')\
           ON DUPLICATE KEY UPDATE guid='${search.cityGuid}', name='${search.city}';`
           );
        search.settlement_id = rows3.insertId;
      } catch(e) {
        logger.debug("SETTLEMENT", e);
        // will be no settlement_id
        // process next org
        return undefined;
      }
    } else {
      search.settlement_id = rows2[0].id;
    }
  }

  return search.settlement_id;
}