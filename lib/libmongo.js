'use strict';

const MONGO = require('mongodb').MongoClient;
var client, collection;


/**
  Create mongo connection
  Return collection promise
  @param dbname {string} Mongo database name
  @param coll {string} Collection name
*/
exports.Connect = async (dbname, coll) => {
  client = await MONGO.connect(
    'mongodb://localhost:27017', { useNewUrlParser: true }
    );
  let dbs = client.db(dbname);
  console.log("MONGO CONNECTED".green);
  collection = dbs.collection(coll);
  return collection;
}


/**
  Mongo close connection
*/
exports.Close = async () => {
  await client.close();
}


/**
  Delete object by `guid` field
  Insert new obj
  @param obj {object} 
*/
exports.DeleteThenInsertObj = async (obj) => {
  if (obj.hasOwnProperty('guid')) {
    await collection.deleteOne({guid: obj['guid']});
    await collection.insertOne(obj);
  }
}