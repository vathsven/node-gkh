'use strict';

const self = this;

var streetTypes = {};
var cityTypes = {};
var areaTypes = {};


/**
  Format house number
  @param orgObj {object} Address object
*/
exports.GetHouseNumber = (addrObj) => {

  var houseNumber, buildingNumber, structNumber, houseGuid;

  if (addrObj.hasOwnProperty('houseNumber')) {
    houseNumber = addrObj['houseNumber'];
  }
  if (addrObj.hasOwnProperty('house') 
    && addrObj['house'].hasOwnProperty('houseNumber')) {
    houseNumber = addrObj['house']['houseNumber'];
    houseGuid = addrObj['house']['guid'];
  }

  if (addrObj.hasOwnProperty('buildingNumber')) {
    buildingNumber = addrObj['buildingNumber'];
  }
  if (addrObj.hasOwnProperty('house') 
    && addrObj['house'].hasOwnProperty('buildingNumber')) {
    buildingNumber = addrObj['house']['buildingNumber'];
  }

  if (addrObj.hasOwnProperty('structNumber')) {
    structNumber = addrObj['structNumber'];
  }
  if (addrObj.hasOwnProperty('house') 
    && addrObj['house'].hasOwnProperty('structNumber')) {
    structNumber = addrObj['house']['structNumber'];
  }

  return [houseNumber, buildingNumber, structNumber];
}

/**
  Flip city, settlement name if they're broken (shortName <-> offName)
  Егорье��ск -> Егорьевск
*/
exports.NameFlipDamaged = (name1, name2) => {

  var buf1 = Buffer.from(name1, 'ascii');
  var buf2 = Buffer.from(name2, 'ascii');

  buf1 = buf1.toJSON(buf1);
  buf2 = buf2.toJSON(buf2);

  var indices1 = [];
  var indices2 = [];
  var element = 253;
  var idx1 = buf1.data.indexOf(element);
  var idx2 = buf2.data.indexOf(element);
  while (idx1 != -1) {
    indices1.push(idx1);
    idx1 = buf1['data'].indexOf(element, idx1 + 1);
  }
  while (idx2 != -1) {
    indices2.push(idx2);
    idx2 = buf2['data'].indexOf(element, idx2 + 1);
  }

  if (indices1.length > 0 && indices2.length > 0) {
    return [name1, name2];
  }

  if (indices1.length > 0) {
    return [name2, name1];
  }
  return [name1, name2];
}


/**
  Translate .json street name -> mysql street name
  @param streetName {string} Street name
*/
exports.FormatStreet = function(streetName) {

  // Format street deprecated
  // Officiall street names
  return streetName;

  var alias = {
    '43-й Армии': '43 Армии',
    '65-летия Победы': '65 лет Победы',
  }
  for (var str in alias) {
    if (streetName == str) {
      return alias[str];
    }
  }
  return streetName;
}


/**
  Parse AdditionalData.organizationOpeningHours
  Extract working hours
  @param work_time {object} Work time
*/
exports.ExtractWorkTime = function(work_time) {
  var week = {
    1: {
      name: "пн",
      lunch: "",
      work: ""
    },
    2: {
      name: "вт",
      lunch: "",
      work: ""
    },
    3: {
      name: "ср",
      lunch: "",
      work: ""
    },
    4: {
      name: "чт",
      lunch: "",
      work: ""
    },
    5: {
      name: "пт",
      lunch: "",
      work: ""
    },
    6: {
      name: "сб",
      lunch: "",
      work: ""
    },
    0: {
      name: "вс",
      lunch: "",
      work: ""
    },
  };

  var html_work = [];

  for (var w in work_time) {

    var work = "";
    var lunch = "";
    var dow = work_time[w]['dayOfWeek'];

    if (work_time[w].hasOwnProperty('openHours')) {
      if (work_time[w]['openHours'].hasOwnProperty('beginDate')) {

        var start = work_time[w]['openHours']['beginDate'];
        var end = work_time[w]['openHours']['endDate'];

        start = String(start).slice(10, 16);
        end = String(end).slice(10, 16);
        var work = start.trim() + " - " + end.trim();
        week[work_time[w]['dayOfWeek']].work = work;
      }
    }
    if (work_time[w].hasOwnProperty('breakHours')) {
      if (work_time[w]['breakHours'].hasOwnProperty('beginDate')) {

        var startl = work_time[w]['breakHours']['beginDate'];
        var endl = work_time[w]['breakHours']['endDate'];

        startl = String(startl).slice(10, 16);
        endl = String(endl).slice(10, 16);
        var lunch = startl.trim() + " - " + endl.trim();
        week[work_time[w]['dayOfWeek']].lunch = lunch;
      }
    }
    if (work.length > 1 && lunch.length < 1) {
      html_work[dow] = "<p>"+week[work_time[w]['dayOfWeek']].name + " " + work+"</p>";
    }
    if (work.length > 1 && lunch.length > 1) {
      html_work[dow] ="<p>"+week[work_time[w]['dayOfWeek']].name + " " + work + " , обед "+ lunch+"</p>";
    }
  }

  return html_work.join("");

}


/**
  Format search parameters to find street_id ...
  @param orgObj {object} Parsed org object
*/
exports.FormatOrgSearch = function(orgObj, logger) {

  var search = null;
  var houseNumber, houseGuid, area, areaType;
  var MISSING_STREET = false;

  if (orgObj.hasOwnProperty('factualAddress')) {

    // No house ??
    if (!orgObj['factualAddress'].hasOwnProperty('house')) {
      if (!orgObj['factualAddress'].hasOwnProperty('houseNumber'))
        return null;
    }
    // House without street ??
    if (!orgObj['factualAddress'].hasOwnProperty('street')) {
      MISSING_STREET = true;
    }
    // No city
    if (orgObj['factualAddress'].hasOwnProperty('city')) {
      search = {
        guid: orgObj['guid'],
        street: MISSING_STREET ? 'NULL' : orgObj['factualAddress']['street']['formalName'],
        streetOff: MISSING_STREET ? null : orgObj['factualAddress']['street']['offName'],
        streetGuid: MISSING_STREET ? null : orgObj['factualAddress']['street']['guid'],
        streetType: MISSING_STREET ? '' : orgObj['factualAddress']['street']['shortName'] + '.',
        //houseNumber: houseNumber,
        houseGuid: orgObj['factualAddress'].hasOwnProperty('house') 
        ? orgObj['factualAddress']['house']['guid'] : undefined,
        city: orgObj['factualAddress']['city']['formalName'],
        cityOff: orgObj['factualAddress']['city']['offName'],
        cityGuid: orgObj['factualAddress']['city']['guid'],
        cityAddr: orgObj['factualAddress']['formattedAddress'],
        regionNumber: orgObj['factualAddress']['region']['regionCode'],
        cityType: orgObj['factualAddress']['city']['shortName'] + '.',
      }
    }
    if (orgObj['factualAddress'].hasOwnProperty('settlement')) {
      search = {
        guid: orgObj['guid'],
        street: MISSING_STREET ? 'NULL' : orgObj['factualAddress']['street']['formalName'],
        streetOff: MISSING_STREET ? null : orgObj['factualAddress']['street']['offName'],
        streetGuid: MISSING_STREET ? null : orgObj['factualAddress']['street']['guid'],
        streetType: MISSING_STREET ? '' : orgObj['factualAddress']['street']['shortName'] + '.',
        //houseNumber: houseNumber,
        houseGuid: orgObj['factualAddress'].hasOwnProperty('house') 
        ? orgObj['factualAddress']['house']['guid'] : undefined,
        city: orgObj['factualAddress']['settlement']['formalName'],
        cityOff: orgObj['factualAddress']['settlement']['offName'],
        cityGuid: orgObj['factualAddress']['settlement']['guid'],
        cityAddr: orgObj['factualAddress']['formattedAddress'],
        regionNumber: orgObj['factualAddress']['region']['regionCode'],
        cityType: orgObj['factualAddress']['settlement']['shortName'] + '.',
      }
    }
    if (!orgObj['factualAddress'].hasOwnProperty('settlement') 
      && !orgObj['factualAddress'].hasOwnProperty('city')) {
        search = {
          guid: orgObj['guid'],
          street: MISSING_STREET ? 'NULL' : orgObj['factualAddress']['street']['formalName'],
          streetOff: MISSING_STREET ? null : orgObj['factualAddress']['street']['offName'],
          streetGuid: MISSING_STREET ? null : orgObj['factualAddress']['street']['guid'],
          streetType: MISSING_STREET ? '' : orgObj['factualAddress']['street']['shortName'] + '.',
          //houseNumber: houseNumber,
          houseGuid: orgObj['factualAddress'].hasOwnProperty('house') 
          ? orgObj['factualAddress']['house']['guid'] : undefined,
          city: orgObj['factualAddress']['region']['formalName'],
          cityOff: orgObj['factualAddress']['region']['offName'],
          cityGuid: orgObj['factualAddress']['region']['guid'],
          cityAddr: orgObj['factualAddress']['formattedAddress'],
          regionNumber: orgObj['factualAddress']['region']['regionCode'],
          cityType: orgObj['factualAddress']['region']['shortName'] + '.',
        }
    }

    [search.houseNumber, search.buildingNumber, search.structNumber] = 
      self.GetHouseNumber(orgObj['factualAddress']);

    // Case:
    // + region, area, street, house
    // - city or settlement
    if (((!orgObj['factualAddress'].hasOwnProperty('settlement') 
      && !orgObj['factualAddress'].hasOwnProperty('city'))) 
      && (orgObj['factualAddress'].hasOwnProperty('region') 
        && orgObj['factualAddress'].hasOwnProperty('area'))) {

      search.city = orgObj['factualAddress']['area']['formalName'];
      search.cityOff = orgObj['factualAddress']['area']['offName'];
      search.cityType = orgObj['factualAddress']['area']['shortName'] + '.';

      [search.area, search.areaBroken] = self.NameFlipDamaged(
          orgObj['factualAddress']['region']['formalName'],
          orgObj['factualAddress']['region']['offName']
        );
      //search.area = orgObj['factualAddress']['region']['formalName'];
      search.areaType = orgObj['factualAddress']['region']['shortName'] + '.';
    }

    // Case:
    // + region, area, city or settlement, street, house
    if ((orgObj['factualAddress'].hasOwnProperty('settlement') 
      || orgObj['factualAddress'].hasOwnProperty('city')) 
      && (orgObj['factualAddress'].hasOwnProperty('region') 
        && orgObj['factualAddress'].hasOwnProperty('area'))) {

      if (orgObj['factualAddress'].hasOwnProperty('city')) {
        search.city = orgObj['factualAddress']['city']['formalName'];
        search.cityOff = orgObj['factualAddress']['city']['offName'];
        search.cityType = orgObj['factualAddress']['city']['shortName'] + '.';
      }

      if (orgObj['factualAddress'].hasOwnProperty('settlement')) {
        search.city = orgObj['factualAddress']['settlement']['formalName'];
        search.cityOff = orgObj['factualAddress']['settlement']['offName'];
        search.cityType = orgObj['factualAddress']['settlement']['shortName'] + '.';
      }

      [search.area, search.areaBroken] = self.NameFlipDamaged(
          orgObj['factualAddress']['region']['formalName'],
          orgObj['factualAddress']['region']['offName']
        );
      //search.area = orgObj['factualAddress']['region']['formalName'];
      search.areaType = orgObj['factualAddress']['region']['shortName'] + '.';
    }
  }

  // authorityFactualAddress preferred
  if (orgObj.hasOwnProperty('authorityFactualAddress')) {

    // No house ??
    if (!orgObj['authorityFactualAddress'].hasOwnProperty('house')) {
      if (!orgObj['authorityFactualAddress'].hasOwnProperty('houseNumber'))
        return null;
    }
    // House without street ??
    if (!orgObj['authorityFactualAddress'].hasOwnProperty('street')) {
      return null;
    }
    // settlement or city ?
    if (orgObj['authorityFactualAddress'].hasOwnProperty('city')) {
      search = {
        guid: orgObj['guid'],
        street: MISSING_STREET ? 'NULL' : orgObj['authorityFactualAddress']['street']['formalName'],
        streetOff: MISSING_STREET ? null : orgObj['authorityFactualAddress']['street']['offName'],
        streetGuid: MISSING_STREET ? null : orgObj['authorityFactualAddress']['street']['guid'],
        streetType: MISSING_STREET ? '' : orgObj['authorityFactualAddress']['street']['shortName'] + '.',
        //houseNumber: houseNumber,
        houseGuid: orgObj['authorityFactualAddress'].hasOwnProperty('house') 
        ? orgObj['authorityFactualAddress']['house']['guid'] : undefined,
        city: orgObj['authorityFactualAddress']['city']['formalName'],
        cityOff: orgObj['authorityFactualAddress']['city']['offName'],
        cityGuid: orgObj['authorityFactualAddress']['city']['guid'],
        cityAddr: orgObj['authorityFactualAddress']['formattedAddress'],
        cityType: orgObj['authorityFactualAddress']['city']['shortName'] + '.',
        regionNumber: orgObj['authorityFactualAddress']['region']['regionCode']
      }
    }
    if (orgObj['authorityFactualAddress'].hasOwnProperty('settlement')) {
      search = {
        guid: orgObj['guid'],
        street: MISSING_STREET ? 'NULL' : orgObj['authorityFactualAddress']['street']['formalName'],
        streetOff: MISSING_STREET ? null : orgObj['authorityFactualAddress']['street']['offName'],
        streetGuid: MISSING_STREET ? null : orgObj['authorityFactualAddress']['street']['guid'],
        streetType: MISSING_STREET ? '' : orgObj['authorityFactualAddress']['street']['shortName'] + '.',
        //houseNumber: houseNumber,
        houseGuid: orgObj['authorityFactualAddress'].hasOwnProperty('house') 
        ? orgObj['authorityFactualAddress']['house']['guid'] : undefined,
        city: orgObj['authorityFactualAddress']['settlement']['formalName'],
        cityOff: orgObj['authorityFactualAddress']['settlement']['offName'],
        cityGuid: orgObj['authorityFactualAddress']['settlement']['guid'],
        cityAddr: orgObj['authorityFactualAddress']['formattedAddress'],
        cityType: orgObj['authorityFactualAddress']['settlement']['shortName'] + '.',
        regionNumber: orgObj['authorityFactualAddress']['region']['regionCode']
      }
    }

    if (!orgObj['authorityFactualAddress'].hasOwnProperty('settlement') 
      && !orgObj['authorityFactualAddress'].hasOwnProperty('city')) {
        search = {
          guid: orgObj['guid'],
          street: MISSING_STREET ? 'NULL' : orgObj['authorityFactualAddress']['street']['formalName'],
          streetOff: MISSING_STREET ? null : orgObj['authorityFactualAddress']['street']['offName'],
          streetGuid: MISSING_STREET ? null : orgObj['authorityFactualAddress']['street']['guid'],
          streetType: MISSING_STREET ? '' : orgObj['authorityFactualAddress']['street']['shortName'] + '.',
          //houseNumber: houseNumber,
          houseGuid: orgObj['authorityFactualAddress'].hasOwnProperty('house') 
          ? orgObj['authorityFactualAddress']['house']['guid'] : undefined,
          city: orgObj['authorityFactualAddress']['region']['formalName'],
          cityOff: orgObj['authorityFactualAddress']['region']['offName'],
          cityGuid: orgObj['authorityFactualAddress']['region']['guid'],
          cityAddr: orgObj['authorityFactualAddress']['formattedAddress'],
          regionNumber: orgObj['authorityFactualAddress']['region']['regionCode'],
          cityType: orgObj['authorityFactualAddress']['region']['shortName'] + '.',
        }
    }

    [search.houseNumber, search.buildingNumber, search.structNumber] = 
      self.GetHouseNumber(orgObj['authorityFactualAddress']);

    // Case:
    // + region, area, street, house
    // - city or settlement
    if (((!orgObj['authorityFactualAddress'].hasOwnProperty('settlement') 
      && !orgObj['authorityFactualAddress'].hasOwnProperty('city'))) 
      && (orgObj['authorityFactualAddress'].hasOwnProperty('region') 
        && orgObj['authorityFactualAddress'].hasOwnProperty('area'))) {

      search.city = orgObj['authorityFactualAddress']['area']['formalName'];
      search.cityOff = orgObj['authorityFactualAddress']['area']['offName'];
      search.cityType = orgObj['authorityFactualAddress']['area']['shortName'] + '.';
      search.cityGuid = orgObj['authorityFactualAddress']['area']['guid'];

      [search.area, search.areaBroken] = self.NameFlipDamaged(
          orgObj['authorityFactualAddress']['region']['formalName'],
          orgObj['authorityFactualAddress']['region']['offName']
        );
      //search.area = orgObj['authorityFactualAddress']['region']['formalName'];
      search.areaType = orgObj['authorityFactualAddress']['region']['shortName'] + '.';
    }

    // Case:
    // + region, area, city or settlement, street, house
    if (((orgObj['authorityFactualAddress'].hasOwnProperty('settlement') 
      || orgObj['authorityFactualAddress'].hasOwnProperty('city'))) 
      && (orgObj['authorityFactualAddress'].hasOwnProperty('region') 
        && orgObj['authorityFactualAddress'].hasOwnProperty('area'))) {

      if (orgObj['authorityFactualAddress'].hasOwnProperty('city')) {
        search.city = orgObj['authorityFactualAddress']['city']['formalName'];
        search.cityOff = orgObj['authorityFactualAddress']['city']['offName'];
        search.cityType = orgObj['authorityFactualAddress']['city']['shortName'] + '.';
        search.cityGuid = orgObj['authorityFactualAddress']['city']['guid'];
      }

      if (orgObj['authorityFactualAddress'].hasOwnProperty('settlement')) {
        search.city = orgObj['authorityFactualAddress']['settlement']['formalName'];
        search.cityOff = orgObj['authorityFactualAddress']['settlement']['offName'];
        search.cityType = orgObj['authorityFactualAddress']['settlement']['shortName'] + '.';
        search.cityGuid = orgObj['authorityFactualAddress']['settlement']['guid'];
      }

      [search.area, search.areaBroken] = self.NameFlipDamaged(
          orgObj['authorityFactualAddress']['region']['formalName'],
          orgObj['authorityFactualAddress']['region']['offName']
        );
      //search.area = orgObj['authorityFactualAddress']['region']['formalName'];
      search.areaType = orgObj['authorityFactualAddress']['region']['shortName'] + '.';
    }

  }

  if (search) {

    search.real_region = 0;
    var settlement_id, street_id, region_id;

    if (search.regionNumber == '50') {
      search.regionNumber = 77;
      search.real_region = 50;
    }

    var re = /\.\./g;

    search.cityType = search.cityType.replace(re, '.');
    if (search.hasOwnProperty('area')) {
      if (search.hasOwnProperty('areaType') && search.areaType !== undefined) {
        search.areaType = search.areaType.replace(re, '.');
      }
    }

    if (!MISSING_STREET) {

      search.streetType = search.streetType.replace(re, '.');

      // Hardcoded streetType
      if (search.streetType == 'ш..') {
        search.streetType = 'ш.';
      }
      if (search.streetType == 'наб..') {
        search.streetType = 'наб.';
      }
      if (search.streetType == 'ал.') {
        search.streetType = 'аллея.';
      }
      if (search.streetType == 'пер..') {
        search.streetType = 'пер.';
      }
      if (search.streetType == 'пр-кт.') {
        search.streetType = 'просп.';
      }
      if (search.streetType == 'тракт.') {
        search.streetType = 'тр.';
      }
      if (search.streetType == 'проезд.') {
        search.streetType = 'пр.';
      }
      if (search.streetType == 'п.') {
        search.streetType = 'пос.';
      }
      if (!streetTypes.hasOwnProperty(search.streetType)) {
        streetTypes[search.streetType] = 1;
      }
    }

    if (!cityTypes.hasOwnProperty(search.cityType)) {
      cityTypes[search.cityType] = 1;
    }
    if (search.areaType !== undefined) {
      if (search.areaType == 'р-н') {
        search.areaType = 'район';
      }
      if (!areaTypes.hasOwnProperty(search.areaType)) {
        areaTypes[search.areaType] = 1;
      }
    }

    var re = /'/g;

    // Compose phones
    if (orgObj['AdditionalData'].hasOwnProperty('dispatcherPhones')) {
      search.phones = orgObj['phone'] + " , " + orgObj['AdditionalData']['dispatcherPhones'];
    } else {
      search.phones = orgObj['phone'];
    }

    if (search.phones === undefined) {
      search.phones = '';
    }
    search.phones = search.phones.replace(re, '');

    // Extract site urls
    if (orgObj['url'] != undefined) {
      search.sites = orgObj['url'];
    } else {
      search.sites = '';
    }
    search.sites = search.sites.replace(re, '');

    // Emails
    if (orgObj['orgEmail'] != undefined) {
      search.emails = orgObj['orgEmail'];
    } else {
      search.emails = '';
    }
    search.emails = search.emails.replace(re, '');

    // TODO: get from organizationRoles[0]['role']['shortName']
    search.org_type_id = self.GetOrgTypeId(orgObj);

    if (orgObj['AdditionalData'].hasOwnProperty('organizationOpeningHours')) {
      search.work_time = self.ExtractWorkTime(orgObj['AdditionalData']['organizationOpeningHours']);
    }
    if (search.work_time === undefined) {
      search.work_time = '';
    }

  }

  return search;
}