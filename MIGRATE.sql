
delete from t4qz8_house where id > 2413828;
delete from t4qz8_house_description where id > 81921;
delete from t4qz8_organization where id > 9404;
delete from t4qz8_organization_employee where id > 7493;
delete from t4qz8_settlement where id > 204398;
delete from t4qz8_street where id > 906825;
delete from t4qz8_sub_region where id > 2675;
delete from t4qz8_organization_house where house_id > 2413828;
delete from t4qz8_organization_house where organization_id > 9404;
delete from t4qz8_organization_type where id > 36;

alter table t4qz8_organization_house add column contract_date DATE default NULL;

# flush guids
alter table t4qz8_settlement modify column name varchar(150);
alter table t4qz8_settlement modify column guid varchar(128) default NULL;
alter table t4qz8_house modify column guid varchar(128) default NULL;
alter table t4qz8_street modify column guid varchar(128) default NULL;
alter table t4qz8_organization modify column guid varchar(128) default NULL;

update t4qz8_settlement set sub_region_id=NULL where sub_region_id>2675;

update t4qz8_settlement set guid=NULL;
update t4qz8_house set guid=NULL;
update t4qz8_street set guid=NULL;
update t4qz8_organization set guid=NULL;
