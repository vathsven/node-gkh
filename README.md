## Deps      

[NodeJS 8.12.0 LTS](https://nodejs.org/en/)

[Yarn](https://yarnpkg.com)

## Install
```bash
yarn
```

## Environment
```bash
# put in ~/.profile or /etc/profile
# FTP
export OCTONICA_FTP_HOST=host
export OCTONICA_FTP_USER=user
export OCTONICA_FTP_PASSWORD=pwd
# MYSQL
export GKH_MYSQL_HOST=host
export GKH_MYSQL_USER=user
export GKH_MYSQL_PASSWORD=pwd
export GKH_MYSQL_DATABASE=db
```

## Migrations

Import MIGRATE.sql

## Run all
```bash
bash run.sh
```

## Manual
## FTP download .zip, extract
```bash
# Download *.zip from ftp folder to `json/zip`
# Extract to `json/extracted`
node ftp.js
```
## Insert / update organizations
```bash
node org.js
```

## Count totals (square, houses ...)

Before running house.js TRUNCATE TABLE t4qz8_count_org or
```bash
node houses.js truncate
```

Update organizations: t4qz8_count_org -> t4qz8_organization
```bash
node house.js org_update_count
```

## Insert / update houses
```bash
# example: node house.js 66
node house.js <region_number>
```

## Logger

node org.js <region_number> -> **org_<region_number>.log**
node house.js <region_number> -> **house_<region_number>.log**

## Tests

```bash
npm test
# or
yarn test
```