FOUND { guid: '3a0e8832-9aba-4f65-bc57-8b1253011148',
  street: 'Северный',
  streetOff: 'Северный',
  streetGuid: '3cb48208-0a6e-4d45-9818-a495b62e7683',
  streetType: 'кв-л.',
  houseGuid: 'd577f45c-fa41-45f7-8139-d7f1dff1ea62',
  city: 'Сапроново',
  cityOff: 'Сапроново',
  cityGuid: 'cee89559-d3c2-4989-86af-eb6ed8d77a9d',
  cityAddr: '142701, обл Московская, р-н Ленинский, д Сапроново, мкр Купелинка, кв-л Северный, д. 5',
  regionNumber: '50',
  cityType: 'д.',
  houseNumber: '5',
  buildingNumber: undefined,
  structNumber: undefined } { guid: '3a0e8832-9aba-4f65-bc57-8b1253011148',
  address: 
   { region: 
      { guid: 'd286798f-0849-4a7c-8e78-33c88dc964c6',
        actual: true,
        aoGuid: '29251dcf-00a1-4e34-98d4-5c47484a36d4',
        aoLevel: 1,
        formalName: 'Московская',
        offName: 'Московская',
        shortName: 'обл',
        regionCode: '50',
        subjectCity: false },
     area: 
      { guid: '79709d0d-e355-487b-bd93-c90c01602738',
        actual: true,
        aoGuid: '69511bb4-8f25-490d-9c54-0c1c00591af2',
        aoLevel: 3,
        formalName: 'Ленинский',
        offName: 'Ленинский',
        shortName: 'р-н',
        parentGuid: '29251dcf-00a1-4e34-98d4-5c47484a36d4',
        regionCode: '50',
        subjectCity: false },
     settlement: 
      { guid: 'cee89559-d3c2-4989-86af-eb6ed8d77a9d',
        actual: true,
        aoGuid: '9a7d0b4d-20c6-4e65-9395-c9be8b2b5ecd',
        aoLevel: 6,
        formalName: 'Сапроново',
        offName: 'Сапроново',
        shortName: 'д',
        parentGuid: '69511bb4-8f25-490d-9c54-0c1c00591af2',
        regionCode: '50',
        subjectCity: false },
     planningStructureElement: 
      { guid: '8c3bbce3-fbe1-4ffb-b897-eda5a9b86d8a',
        actual: true,
        aoGuid: '6d69d8e2-b09b-4874-94ef-7e3a5238d7d3',
        aoLevel: 65,
        formalName: 'Купелинка',
        offName: 'Купелинка',
        shortName: 'мкр',
        parentGuid: '9a7d0b4d-20c6-4e65-9395-c9be8b2b5ecd',
        regionCode: '50',
        subjectCity: false },
     street: 
      { guid: '3cb48208-0a6e-4d45-9818-a495b62e7683',
        actual: true,
        aoGuid: 'f5bfce72-1f71-4919-97cc-78a1eaafd640',
        aoLevel: 7,
        formalName: 'Северный',
        offName: 'Северный',
        shortName: 'кв-л',
        parentGuid: '6d69d8e2-b09b-4874-94ef-7e3a5238d7d3',
        regionCode: '50',
        subjectCity: false },
     house: 
      { guid: 'd577f45c-fa41-45f7-8139-d7f1dff1ea62',
        actual: true,
        houseGuid: 'd4e76347-cb11-411f-9d6f-6a3b576df490',
        aoGuid: 'f5bfce72-1f71-4919-97cc-78a1eaafd640',
        postalCode: '142701',
        houseNumber: '5',
        isAddedManually: false,
        estStatus: 'DOM',
        strStatus: 'NE_OPREDELENO',
        aggregated: false,
        houseTextAddress: '5' },
     formattedAddress: '142701, обл Московская, р-н Ленинский, д Сапроново, мкр Купелинка, кв-л Северный, д. 5',
     deprecated: false },
  houseType: 
   { guid: '55a9b133-24eb-456f-85c4-134624646a8d',
     code: '1',
     rootEntityGuid: '55a9b133-24eb-456f-85c4-134624646a8d',
     actual: true,
     lastUpdateDate: '06.02.2015',
     createDate: '06.02.2015',
     houseTypeName: 'Многоквартирный' },
  houseUid: 'Toe00533',
  totalSquare: '17807.2',
  residentialSquare: '13174.4',
  houseCondition: 
   { guid: '2d3ae73e-6c72-4740-9122-9c632d1893a7',
     code: '2',
     rootEntityGuid: '2d3ae73e-6c72-4740-9122-9c632d1893a7',
     actual: true,
     lastUpdateDate: '06.02.2015',
     createDate: '06.02.2015',
     houseCondition: 'Исправный' },
  deterioration: '0',
  buildingYear: '2013',
  operationYear: 2013,
  planSeries: 'П44К',
  intWallMaterialList: 'Стены из несущих панелей',
  energyInspectionDate: '13.11.2013',
  municipalityOrganizationList: 
   [ { guid: '1c09593e-1cd2-45a8-aa7b-53eb87d76ab5',
       fullName: 'АДМИНИСТРАЦИЯ МУНИЦИПАЛЬНОГО ОБРАЗОВАНИЯ ЛЕНИНСКИЙ МУНИЦИПАЛЬНЫЙ РАЙОН МОСКОВСКОЙ ОБЛАСТИ',
       shortName: 'АДМИНИСТРАЦИЯ ЛЕНИНСКОГО РАЙОНА МОСКОВСКОЙ ОБЛАСТИ',
       orgAddress: '142703, Московская обл, Ленинский р-н, г Видное, ул Школьная, д. 26А',
       organizationType: 'A',
       registryOrganizationRootEntityGuid: '01f6e96a-6f4d-48ce-8221-0b8eb428ad60',
       ogrn: '1025000661421' },
     { guid: '1c09593e-1cd2-45a8-aa7b-53eb87d76ab5',
       fullName: 'АДМИНИСТРАЦИЯ МУНИЦИПАЛЬНОГО ОБРАЗОВАНИЯ ЛЕНИНСКИЙ МУНИЦИПАЛЬНЫЙ РАЙОН МОСКОВСКОЙ ОБЛАСТИ',
       shortName: 'АДМИНИСТРАЦИЯ ЛЕНИНСКОГО РАЙОНА МОСКОВСКОЙ ОБЛАСТИ',
       orgAddress: '142703, Московская обл, Ленинский р-н, г Видное, ул Школьная, д. 26А',
       organizationType: 'A',
       registryOrganizationRootEntityGuid: '01f6e96a-6f4d-48ce-8221-0b8eb428ad60',
       ogrn: '1025000661421' } ],
  canceledOrAsyncProcessed: false,
  houseGuid: 'd577f45c-fa41-45f7-8139-d7f1dff1ea62',
  residentialPremiseCount: '246',
  nonResidentialPremiseCount: '6',
  municipalityOrganization: 
   { guid: '1c09593e-1cd2-45a8-aa7b-53eb87d76ab5',
     fullName: 'АДМИНИСТРАЦИЯ МУНИЦИПАЛЬНОГО ОБРАЗОВАНИЯ ЛЕНИНСКИЙ МУНИЦИПАЛЬНЫЙ РАЙОН МОСКОВСКОЙ ОБЛАСТИ',
     shortName: 'АДМИНИСТРАЦИЯ ЛЕНИНСКОГО РАЙОНА МОСКОВСКОЙ ОБЛАСТИ',
     organizationType: 'A',
     okopf: { guid: '9b7a0af3-5ebb-4747-8144-389096c20553', code: '75404' },
     ogrn: '1025000661421' },
  maxFloorCount: '18',
  ableEnterDeviceMetering: false,
  blockedHouse: false }
